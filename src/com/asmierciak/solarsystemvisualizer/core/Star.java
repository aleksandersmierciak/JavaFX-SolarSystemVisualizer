package com.asmierciak.solarsystemvisualizer.core;

/**
 * This class represents a star model.
 */
public class Star extends AstronomicalObject {
    /**
     * Initializes a new instance of the Star class. Sets the name of this object as well as the
     * bounding object.
     * @param name Name for the new object.
     */
    public Star(final String name,
                final PhysicalCharacteristics physicalCharacteristics,
                final OrbitalCharacteristics orbitalCharacteristics) {
        super(name, physicalCharacteristics, orbitalCharacteristics);
    }
}
