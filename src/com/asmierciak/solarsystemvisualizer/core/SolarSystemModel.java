package com.asmierciak.solarsystemvisualizer.core;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This class represents a Solar System data model.
 */
public class SolarSystemModel implements StarSystemModel {
    private Map<SolarSystemObject, AstronomicalObject> objects = new HashMap<>();

    /**
     * Initializes a new instance of the SolarSystemModel class. Reads data from the JSON
     * document and creates models.
     */
    public SolarSystemModel() {
        JSONParser parser = new JSONParser();
        try {
            String dataPath = getClass().getResource("SolarSystemData.json").getPath();
            Object obj = parser.parse(new FileReader(dataPath));
            JSONObject solarSystemData = (JSONObject) obj;

            JSONObject sunData = (JSONObject) solarSystemData.get("Star");
            createSun(sunData);
            JSONArray planetsData = (JSONArray) solarSystemData.get("Planets");
            createPlanets(planetsData);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Creates the Solar System star (the Sun) model. Data is being read from the JSON object given
     * as a parameter.
     * @param sunData Data for the Sun model, in a JSON object.
     */
    private void createSun(JSONObject sunData) {
        PhysicalCharacteristics physicalCharacteristics = readPhysicalCharacteristics(sunData);
        Star sun = new Star((String)sunData.get("name"), physicalCharacteristics, null);
        objects.put(SolarSystemObject.valueOf(sun.getName()), sun);
    }

    /**
     * Creates the Solar System planets. Data is being read from the JSON array given as a
     * parameter.
     * @param planetsData Data for the planets models, in a JSON array.
     */
    private void createPlanets(JSONArray planetsData) {
        for (Object object : planetsData) {
            JSONObject planetData = (JSONObject) object;
            createPlanet(planetData);
        }
    }

    /**
     * Creates a Solar System planet. Data is being read from the JSON object given as a parameter.
     * @param planetData Data for the planet model, in a JSON object.
     */
    private void createPlanet(JSONObject planetData) {
        PhysicalCharacteristics physicalCharacteristics = readPhysicalCharacteristics(planetData);
        OrbitalCharacteristics orbitalCharacteristics = readOrbitCharacteristics((JSONObject)planetData
                .get("orbit"));
        Planet planet = new Planet((String)planetData.get("name"), physicalCharacteristics,
                orbitalCharacteristics);
        objects.put(SolarSystemObject.valueOf(planet.getName()), planet);
    }

    /**
     * Reads physical characteristics held in a JSON object given as a parameter.
     * @param source Data with physical characteristics, in a JSON object.
     * @return New PhysicalCharacteristics object.
     */
    private PhysicalCharacteristics readPhysicalCharacteristics(JSONObject source) {
        return new PhysicalCharacteristics
                .Builder(((Number)source.get("radius")).doubleValue())
                .mass(((Number) source.get("mass")).doubleValue())
                .build();
    }

    /**
     * Reads orbital characteristics held in a JSON object given as a parameter.
     * @param source Data with orbital characteristics, in a JSON object.
     * @return New OrbitalCharacteristics object.
     */
    private OrbitalCharacteristics readOrbitCharacteristics(JSONObject source) {
        return new OrbitalCharacteristics
                .Builder(objects.get(SolarSystemObject.Sun))
                .aphelion(((Number) source.get("aphelion")).doubleValue())
                .perihelion(((Number) source.get("perihelion")).doubleValue())
                .orbitalPeriod(((Number) source.get("orbitalPeriod")).doubleValue())
                .averageOrbitalSpeed(((Number) source.get("averageOrbitalSpeed")).doubleValue())
                .build();
    }

    /**
     * Gets the Solar System star (the Sun) model.
     * @return Sun model.
     */
    @Override
    public Star getStar() {
        return (Star)objects.get(SolarSystemObject.Sun);
    }

    /**
     * Gets models of all planets.
     * @return Planets models.
     */
    @Override
    public List<Planet> getPlanets() {
        List<Planet> planets = new LinkedList<>();
        for (AstronomicalObject object : objects.values()) {
            if (Planet.class.isAssignableFrom(object.getClass())) {
                planets.add((Planet)object);
            }
        }
        return planets;
    }

    /**
     * Gets model of an astronomical object of name given as a parameter.
     * @param name Name of the astronomical object.
     * @return Astronomical object model.
     */
    @Override
    public AstronomicalObject getObject(String name) {
        return objects.get(SolarSystemObject.valueOf(name));
    }

    /**
     * Gets properties' map of an astronomical object of name given as a parameter.
     * @param name Name of the astronomical object.
     * @return Astronomical object model properties' map.
     */
    @Override
    public Map<String, String> getProperties(String name) {
        return getObject(name).getProperties();
    }

    /**
     * Gets the names of Solar System objects as an array of Strings.
     * @return Names of Solar System objects as an array of Strings.
     */
    @Override
    public String[] getObjectNames() {
        return SolarSystemObject.names();
    }

    /**
     * Parses the astronomical object name out of some text given as a parameter.
     * @param text Some text that supposedly contains an astronomical object name.
     * @return Name of the astronomical object, found in source text.
     */
    @Override
    public String parseObjectName(String text) {
        if (text.toLowerCase().contains("sun")) {
            return SolarSystemObject.Sun.name();
        } else if (text.toLowerCase().contains("mercury")) {
            return SolarSystemObject.Mercury.name();
        } else if (text.toLowerCase().contains("venus")) {
            return SolarSystemObject.Venus.name();
        } else if (text.toLowerCase().contains("earth")) {
            return SolarSystemObject.Earth.name();
        } else if (text.toLowerCase().contains("mars")) {
            return SolarSystemObject.Mars.name();
        } else if (text.toLowerCase().contains("belt")) {
            return SolarSystemObject.Belt.name();
        } else if (text.toLowerCase().contains("jupiter")) {
            return SolarSystemObject.Jupiter.name();
        } else if (text.toLowerCase().contains("saturn")) {
            return SolarSystemObject.Saturn.name();
        } else if (text.toLowerCase().contains("uranus")) {
            return SolarSystemObject.Uranus.name();
        } else if (text.toLowerCase().contains("neptune")) {
            return SolarSystemObject.Neptune.name();
        } else throw new IllegalArgumentException();
    }

    /**
     * Gets the astronomical object title. Used for pretty-printing purposes.
     * @param name Name of the astronomical object.
     * @return Title of the astronomical object.
     */
    @Override
    public String getObjectTitle(String name) {
        return SolarSystemObject.valueOf(name).toString();
    }
}
