package com.asmierciak.solarsystemvisualizer.core;

/**
 * This enum represents astronomical objects found in the Solar System.
 */
public enum SolarSystemObject {
    Sun("Sun"),
    Mercury("Mercury"),
    Venus("Venus"),
    Earth("Earth"),
    Mars("Mars"),
    Belt("Asteroid belt"),
    Jupiter("Jupiter"),
    Saturn("Saturn"),
    Uranus("Uranus"),
    Neptune("Neptune");

    /**
     * Name for the enum value.
     */
    private String label;

    /**
     * Initializes a new enum with the value set.
     * @param label Value for the new enum.
     */
    private SolarSystemObject(String label) {
        this.label = label;
    }

    /**
     * Gets the names held in an enum as an array of strings.
     * @return Array of enum names.
     */
    public static String[] names() {
        SolarSystemObject[] objects = values();
        String[] names = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            names[i] = objects[i].name();
        }
        return names;
    }

    /**
     * Gets this enum value's string representation (label).
     * @return String representation of this enum value.
     */
    @Override
    public String toString() {
        return this.label;
    }
}
