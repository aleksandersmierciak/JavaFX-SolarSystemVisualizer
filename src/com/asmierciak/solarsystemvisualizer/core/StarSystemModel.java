package com.asmierciak.solarsystemvisualizer.core;

import java.util.List;
import java.util.Map;

/**
 * This interface represents a Star System data model. It lists methods
 * expected in such a model to ensure ease of data exchange.
 */
public interface StarSystemModel {
    Star getStar();

    List<Planet> getPlanets();

    AstronomicalObject getObject(String name);

    Map<String, String> getProperties(String name);

    String[] getObjectNames();

    String parseObjectName(String text);

    String getObjectTitle(String name);
}
