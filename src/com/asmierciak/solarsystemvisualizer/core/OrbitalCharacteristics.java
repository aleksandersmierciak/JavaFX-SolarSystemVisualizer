package com.asmierciak.solarsystemvisualizer.core;

import java.util.LinkedHashMap;
import java.util.Map;

public class OrbitalCharacteristics {
    /**
     * The astronomical object that the orbiting object belongs to (orbits).
     */
    private AstronomicalObject boundingObject;

    /**
     * The maximal distance between the orbiting object and the bounding astronomical object that
     * it orbits.
     * Given in astronomical units (AU).
     * Also known as elliptical major radius, apoapsis, apocentre, or apapsis.
     */
    private double aphelion;

    /**
     * The minimal distance between the orbiting object and the bounding astronomical object that
     * it orbits.
     * Given in astronomical units (AU).
     * Also known as elliptical minor radius, periapsis, or pericentre.
     */
    private double perihelion;

    /**
     * The time needed for the orbiting object to make one complete orbit about the bounding
     * astronomical object that it orbits.
     * Given in Julian years (a, yr).
     */
    private double orbitalPeriod;

    /**
     * The average orbital speed of the orbiting object.
     * Given in kilometers per second (km/s).
     */
    private double averageOrbitalSpeed;

    public Map<String, String> getProperties() {
        Map<String, String> properties = new LinkedHashMap<>();
        properties.put("boundingObject", boundingObject.getName());
        properties.put("aphelion", String.valueOf(aphelion));
        properties.put("perihelion", String.valueOf(perihelion));
        properties.put("orbitalPeriod", String.valueOf(orbitalPeriod));
        properties.put("averageOrbitalSpeed", String.valueOf(averageOrbitalSpeed));
        return properties;
    }

    public AstronomicalObject getBoundingObject() {
        return boundingObject;
    }

    public double getAphelion() {
        return aphelion;
    }

    public double getPerihelion() {
        return perihelion;
    }

    public double getOrbitalPeriod() {
        return orbitalPeriod;
    }

    public double getAverageOrbitalSpeed() {
        return averageOrbitalSpeed;
    }

    public static class Builder {
        private AstronomicalObject boundingObject;

        private double aphelion;

        private double perihelion;

        private double orbitalPeriod;

        private double averageOrbitalSpeed;

        /**
         * Initializes a new instance of the Builder class. Sets the mandatory bounding object for
         * the to-be-built OrbitalCharacteristics object.
         * @param boundingObject Bounding object (the one that is orbited).
         */
        public Builder(AstronomicalObject boundingObject) {
            this.boundingObject = boundingObject;
        }

        /**
         * Sets the maximal distance between the orbiting object and the bounding astronomical
         * object that it orbits (given in astronomical units).
         * @param aphelion Maximal distance between the orbiting object and the bounding
         *                 astronomical object that it orbits.
         */
        public Builder aphelion(double aphelion) {
            this.aphelion = aphelion;
            return this;
        }

        /**
         * Sets the minimal distance between the orbiting object and the bounding astronomical
         * object that it orbits (given in astronomical units).
         * @param perihelion Minimal distance between the orbiting object and the bounding
         *                   astronomical object that it orbits.
         */
        public Builder perihelion(double perihelion) {
            this.perihelion = perihelion;
            return this;
        }

        /**
         * Sets the time needed for the orbiting object to make one complete orbit about the
         * bounding astronomical object that it orbits (given in Julian years).
         * @param orbitalPeriod Time needed for the orbiting object to make one complete
         *                      orbit about the bounding astronomical object that it orbits.
         */
        public Builder orbitalPeriod(double orbitalPeriod) {
            this.orbitalPeriod = orbitalPeriod;
            return this;
        }

        /**
         * Sets the average orbital speed of the orbiting object (given in kilometers per second).
         * @param averageOrbitalSpeed Average orbital speed of the orbiting object.
         */
        public Builder averageOrbitalSpeed(double averageOrbitalSpeed) {
            this.averageOrbitalSpeed = averageOrbitalSpeed;
            return this;
        }

        /**
         * Creates a new PhysicalCharacteristics object with the data set to this Builder object.
         * @return A new PhysicalCharacteristics object.
         */
        public OrbitalCharacteristics build() {
            return new OrbitalCharacteristics(this);
        }
    }

    /**
     * Initializes a new instance of the OrbitalCharacteristics class. Sets all the fields using
     * the data set to the Builder object given as a parameter.
     * @param builder Builder object from which data will be used.
     */
    private OrbitalCharacteristics(Builder builder) {
        this.boundingObject = builder.boundingObject;
        this.aphelion = builder.aphelion;
        this.perihelion = builder.perihelion;
        this.orbitalPeriod = builder.orbitalPeriod;
        this.averageOrbitalSpeed = builder.averageOrbitalSpeed;
    }
}
