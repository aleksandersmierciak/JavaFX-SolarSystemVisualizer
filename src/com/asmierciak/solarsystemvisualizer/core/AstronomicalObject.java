package com.asmierciak.solarsystemvisualizer.core;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class AstronomicalObject {
    /**
     * Name of this astronomical object.
     */
    private final String name;

    private final PhysicalCharacteristics physicalCharacteristics;

    /**
     * The orbit of this astronomical object.
     */
    private OrbitalCharacteristics orbit;

    /**
     * The astronomical objects that orbit this object.
     */
    private final List<AstronomicalObject> satellites = new LinkedList<>();

    /**
     * Initializes a new instance of the AstronomicalObject class. Sets the name of the
     * new object and its physical properties, such as radius, mass and so on.
     * @param name Name for the new object.
     * @param physicalCharacteristics Physical characteristics for the new object.
     */
    public AstronomicalObject(final String name,
                              final PhysicalCharacteristics physicalCharacteristics) {
        this.name = name;
        this.physicalCharacteristics = physicalCharacteristics;
    }

    /**
     * Initializes a new instance of the AstronomicalObject class. Sets the name of the
     * new object and its physical properties, such as radius, mass and so on.
     * @param name Name for the new object.
     * @param physicalCharacteristics Physical characteristics for the new object.
     * @param orbitalCharacteristics Orbit characteristics for the new object.
     */
    public AstronomicalObject(final String name,
                              final PhysicalCharacteristics physicalCharacteristics,
                              final OrbitalCharacteristics orbitalCharacteristics) {
        this.name = name;
        this.physicalCharacteristics = physicalCharacteristics;
        this.orbit = orbitalCharacteristics;
    }

    public Map<String, String> getProperties() {
        Map<String, String> properties = new LinkedHashMap<>();

        properties.put("name", name);
        properties.putAll(physicalCharacteristics.getProperties());

        if (orbit != null) {
            properties.putAll(orbit.getProperties());
        }

        if (satellites != null) {
            int i = 0;
            for (AstronomicalObject satellite : satellites) {
                properties.put("Satellite #" + i++, satellite.getName());
            }
        }

        return properties;
    }

    public String getName() {
        return name;
    }

    public double getRadius() { return physicalCharacteristics.getRadius(); }

    public OrbitalCharacteristics getOrbit() {
        return orbit;
    }

    public void setOrbit(OrbitalCharacteristics orbit) {
        this.orbit = orbit;
    }

    public List<AstronomicalObject> getSatellites() {
        return satellites;
    }

    public void addSatellite(AstronomicalObject satellite) {
        satellites.add(satellite);
    }

    public void removeSatellite(AstronomicalObject satellite) {
        satellites.remove(satellite);
    }
}
