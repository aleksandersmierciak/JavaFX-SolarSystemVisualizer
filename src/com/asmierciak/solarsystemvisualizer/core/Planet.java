package com.asmierciak.solarsystemvisualizer.core;

/**
 * This class represents a planet model.
 */
public class Planet extends AstronomicalObject {
    /**
     * Initializes a new instance of the Planet class. Sets the name of this object as well as
     * the bounding object.
     * @param name Name for the new object.
     * @param physicalCharacteristics
     * @param orbitalCharacteristics
     */
    public Planet(final String name,
                  final PhysicalCharacteristics physicalCharacteristics,
                  final OrbitalCharacteristics orbitalCharacteristics) {
        super(name, physicalCharacteristics, orbitalCharacteristics);
    }
}
