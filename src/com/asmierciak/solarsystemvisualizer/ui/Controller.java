package com.asmierciak.solarsystemvisualizer.ui;

import com.asmierciak.solarsystemvisualizer.core.AstronomicalObject;
import com.asmierciak.solarsystemvisualizer.core.OrbitalCharacteristics;
import com.asmierciak.solarsystemvisualizer.core.SolarSystemModel;
import com.asmierciak.solarsystemvisualizer.core.StarSystemModel;
import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Ellipse;
import javafx.util.Duration;

import java.util.*;

/**
 * This class represents a main controller of the SolarSystemVisualizer application.
 * It handles the main window controls as well as the astronomical objects graphical
 * representations.
 */
public class Controller {
    /**
     * Minimum scale value of the canvas.
     */
    private static final double MINIMUM_SCALE = 0.01;

    /**
     * Maximum scale value of the canvas.
     */
    private static final double MAXIMUM_SCALE = 2.0;

    /**
     * Scale coefficient used in conjunction with scroll event delta.
     */
    private static final double SCALE_COEFFICIENT = 0.001;

    /**
     * Orbital distance multiplier.
     */
    private static final double ORBIT_LENGTH_MULTIPLIER = 70.0;

    /**
     * Arbitrarily set center of the planetary system (position of the star).
     */
    private static final Point2D STAR_SYSTEM_CENTER = new Point2D(0.0, 250.0);

    /**
     * Indicates whether the orbiting animation is currently running.
     */
    private boolean isPlaying;

    /**
     * Planetary system model used to initialize the view.
     */
    private StarSystemModel starSystem = new SolarSystemModel();

    /**
     * Canvas for displaying planetary system model.
     */
    @FXML
    private Pane canvas;

    /**
     * Choice box with astronomical objects' names in it.
     */
    @FXML
    private ChoiceBox<String> objectsBox;

    /**
     * Pane displaying properties of the currently selected astronomical object.
     */
    @FXML
    private GridPane propertiesPane;

    /**
     * Astronomical objects' images loaded from the filesystem.
     */
    private Map<String, Image> astronomicalObjectImages = new LinkedHashMap<>();

    /**
     * Image of the center object of the planetary system.
     */
    private ImageView star;

    /**
     * Transitions (orbiting animation) of all the astronomical objects loaded.
     */
    private List<PathTransition> objectTransitions = new LinkedList<>();

    /**
     * Toggles all object transitions on or off.
     */
    public void simulate() {
        if (isPlaying = !isPlaying) {
            for (PathTransition transition : objectTransitions) {
                transition.play();
            }
        } else {
            for (PathTransition transition : objectTransitions) {
                transition.pause();
            }
        }
    }

    /**
     * Initializes this controller object. Populates the canvas with
     * properly set Star System model.
     */
    public void initialize() {
        initializeCanvas();
        initializeObjectImages();
        initializeStar();
        initializePlanets();
        initializeChoiceBox();
    }

    /**
     * Initializes the canvas object; sets initial scale and translation.
     */
    private void initializeCanvas() {
        canvas.scaleYProperty().bind(canvas.scaleXProperty());
        canvas.setScaleX(0.1);
        canvas.setTranslateX(-200.0);
    }

    /**
     * Initializes images of all astronomical objects retrieved
     * from the Star System model object.
     */
    private void initializeObjectImages() {
        for (String name : starSystem.getObjectNames()) {
            try {
                loadObjectLocalImage(name);
            } catch (IllegalArgumentException e) {
                System.out.println(
                        "Could not locate resource for Star System object of name " + name);
            }
        }
    }

    /**
     * Loads a local image for the astronomical object of name given as a parameter.
     * @param name Astronomical object unique name.
     */
    private void loadObjectLocalImage(String name) {
        astronomicalObjectImages.put(name,
                new Image("com/asmierciak/solarsystemvisualizer/ui/resources/" + name + ".png"));
    }

    /**
     * Initializes center object of the Star System.
     */
    private void initializeStar() {
        AstronomicalObject model = starSystem.getStar();
        if (model != null) {
            star = createObjectImage(model.getName());
            setImageSize(star, model);
            setImagePosition(star, model);
            canvas.getChildren().add(star);
        }
    }

    /**
     * Initializes planets of the Star System.
     */
    private void initializePlanets() {
        for (AstronomicalObject object : starSystem.getPlanets()) {
            initializeObject(object);
        }
    }

    /**
     * Initializes the choice box, populating it with astronomical object's names.
     */
    private void initializeChoiceBox() {
        ObservableList<String> list = new SimpleListProperty<>(FXCollections.observableList(new
                ArrayList<>()));
        list.addAll(astronomicalObjectImages.keySet());
        objectsBox.setItems(list);
        if (list.size() > 0) {
            objectsBox.setValue(list.get(0));
        }
    }

    /**
     * Toggles visibility of astronomical object of name given as a parameter.
     * @param name Astronomical object unique name.
     */
    private void toggleObjectVisibility(String name) {
        Node imageView = canvas.lookup("#" + name);
        imageView.setVisible(!imageView.isVisible());
    }

    /**
     * Initializes an astronomical object graphical representation based on the data model given
     * as a parameter.
     * @param model Astronomical object data model.
     */
    private void initializeObject(AstronomicalObject model) {
        ImageView imageView = createObjectImage(model.getName());
        setImageSize(imageView, model);
        setImagePosition(imageView, model);

        OrbitalCharacteristics orbitalCharacteristics = model.getOrbit();
        if (orbitalCharacteristics != null) {
            applyOrbitalTransition(orbitalCharacteristics, imageView);
        } else {
            star = imageView;
        }
        canvas.getChildren().add(imageView);
    }

    /**
     * Creates an image view for the astronomical object of name given as a
     * parameter.
     * @param name Astronomical object unique name.
     * @return Image view for the astronomical object specified.
     */
    private ImageView createObjectImage(String name) {
        ImageView objectImage = new ImageView();
        objectImage.setId(name);
        objectImage.setOnMouseClicked(event -> displayProperties(name));
        objectImage.setCache(true);
        objectImage.setPreserveRatio(true);
        objectImage.setImage(astronomicalObjectImages.get(name));
        return objectImage;
    }

    /**
     * Displays the properties of the astronomical object of name given as a parameter.
     * @param object Astronomical object unique name.
     */
    private void displayProperties(String object) {
        Map<String, String> characteristics = starSystem.getProperties(object);
        propertiesPane.getChildren().clear();
        int i = 0;
        for (String key : characteristics.keySet()) {
            propertiesPane.addRow(i++, new Label(key), new Label(characteristics.get(key)));
        }
    }

    /**
     * Sets the size of the image view based on the data from the model
     * given as a parameter.
     * @param imageView Image view for an astronomical object.
     * @param model Astronomical object data model.
     */
    private void setImageSize(ImageView imageView, AstronomicalObject model) {
        imageView.setFitHeight(model.getRadius() / 300.0);
    }

    /**
     * Sets the position of the image view based on the data from the model
     * given as a parameter.
     * @param imageView Image view for an astronomical object.
     * @param model Astronomical object data model.
     */
    private void setImagePosition(ImageView imageView, AstronomicalObject model) {
        if (model.getOrbit() != null) {
            imageView.setX(
                    star.getX() + star.getBoundsInLocal().getWidth() +
                            model.getOrbit().getAphelion() * ORBIT_LENGTH_MULTIPLIER -
                            imageView.getBoundsInLocal().getWidth() / 2.0);
            imageView.setY(
                    star.getY() + star.getBoundsInLocal().getHeight() / 2.0 -
                            imageView.getBoundsInLocal().getHeight() / 2.0);
            imageView.setRotate(90.0);
            imageView.setTranslateX(star.getTranslateX());
            imageView.setTranslateY(star.getTranslateY());
        } else {
            imageView.setX(STAR_SYSTEM_CENTER.getX() -
                    imageView.getBoundsInLocal().getWidth() / 2.0);
            imageView.setY(STAR_SYSTEM_CENTER.getY() -
                    imageView.getBoundsInLocal().getHeight() / 2.0);
        }
    }

    /**
     * Applies an orbital transition to the image view representing an astronomical
     * object.
     * The orbit is specified by an ellipse based on the data model given as a
     * parameter.
     * @param orbital Astronomical object orbital characteristics.
     * @param imageView Image view for an astronomical object.
     */
    private void applyOrbitalTransition(OrbitalCharacteristics orbital,
                                        ImageView imageView) {
        PathTransition transition = new PathTransition();
        transition.setPath(createEllipticalOrbit(orbital));
        transition.setNode(imageView);
        transition.setInterpolator(Interpolator.LINEAR);
        transition.setDuration(Duration.seconds(orbital.getOrbitalPeriod()));
        transition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        transition.setCycleCount(Timeline.INDEFINITE);
        objectTransitions.add(transition);
    }

    /**
     * Creates an ellipse representing an astronomical object orbit based on the data given as a
     * parameter.
     * @param orbital Astronomical object orbital characteristics.
     * @return Ellipse representing an astronomical object orbit.
     */
    private Ellipse createEllipticalOrbit(OrbitalCharacteristics orbital) {
        Ellipse ellipse = new Ellipse();
        ellipse.setCenterX(star.getX() + star.getBoundsInLocal().getWidth() / 2.0);
        ellipse.setCenterY(star.getY() + star.getBoundsInLocal().getHeight() / 2.0);
        ellipse.translateXProperty().bind(star.translateXProperty());
        ellipse.translateYProperty().bind(star.translateYProperty());
        ellipse.setRadiusX(star.getBoundsInLocal().getWidth() / 2.0 +
                orbital.getAphelion() * ORBIT_LENGTH_MULTIPLIER);
        ellipse.setRadiusY(star.getBoundsInLocal().getHeight() / 2.0 +
                orbital.getPerihelion() * ORBIT_LENGTH_MULTIPLIER);
        return ellipse;
    }

    /**
     * Handles mouse scroll action by applying canvas scaling (when Ctrl key down) or
     * moving the canvas horizontally (with Shift key down) or vertically (without Shift key down).
     * @param event Mouse scroll event parameters.
     */
    public void handleScroll(ScrollEvent event) {
        if (event.isControlDown()) {
            scaleCanvas(event.getDeltaY());
        } else {
            moveCanvas(event.getDeltaY(), event.isShiftDown());
        }
    }

    /**
     * Scales the canvas based on the value given as a parameter. Positive values make the scale
     * greater (zoom in), negative values make the scale lower (zoom out).
     * @param value Scale change delta.
     */
    private void scaleCanvas(double value) {
        double newScale = canvas.getScaleX() + value * SCALE_COEFFICIENT;
        if (value < 0.0 && newScale > MINIMUM_SCALE ||
                value >= 0.0 && newScale < MAXIMUM_SCALE) {
            double change = newScale / canvas.getScaleX();
            canvas.setTranslateX(canvas.getTranslateX() * change);
            canvas.setTranslateY(canvas.getTranslateY() * change);

            canvas.setScaleX(newScale);
        }
    }

    /**
     * Moves the canvas around, horizontally or vertically.
     * @param delta Move change delta.
     * @param horizontal Whether to move horizontally or vertically.
     */
    private void moveCanvas(double delta, boolean horizontal) {
        if (horizontal) {
            canvas.setTranslateX(canvas.getTranslateX() + delta);
        } else {
            canvas.setTranslateY(canvas.getTranslateY() + delta);
        }
    }

    /**
     * Gets the previous astronomical object.
     */
    public void getPrevious() {
        int index = getCurrentChoiceIndex();
        if (index > 0) {
            objectsBox.setValue(objectsBox.getItems().get(index - 1));
        }
    }

    /**
     * Gets the next astronomical object.
     */
    public void getNext() {
        int index = getCurrentChoiceIndex();
        if (index < objectsBox.getItems().size() - 1) {
            objectsBox.setValue(objectsBox.getItems().get(index + 1));
        }
    }

    /**
     * Gets the index of currently chosen value in the astronomical objects' names box.
     * @return Index of currently chosen value.
     */
    private int getCurrentChoiceIndex() {
        ObservableList<String> objectNames = objectsBox.getItems();
        int index = objectNames.indexOf(objectsBox.getValue());
        if (index == -1) {
            throw new IllegalArgumentException("Object name not found in the list");
        }
        return index;
    }

    /**
     * Toggles the currently chosen object's visibility.
     */
    public void toggleVisibility() {
        toggleObjectVisibility(objectsBox.getValue());
    }
}
